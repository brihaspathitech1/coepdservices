﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Coepdservices.DataAccessLayer
{
    public class DataQueries
    {
        public DataQueries()
        { }
        public static DataSet Selectcomm(string Strquery)
        {
            DataSet ds = new DataSet();
            SqlConnection con = dataconnection.GetConnection();
            SqlDataAdapter da = new SqlDataAdapter(Strquery,con);
            da.Fill(ds);
            return ds;
        }
        public static void Insertcomm(string Strquery)
        {
            SqlConnection con = dataconnection.GetConnection();
            SqlCommand cmd = new SqlCommand(Strquery);
            cmd.Connection = con;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            
        }
    }
   
}