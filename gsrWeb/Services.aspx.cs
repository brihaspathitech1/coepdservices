﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Coepdservices.DataAccessLayer;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Net;

namespace Coepdservices
{
    public partial class Services : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if(Request.HttpMethod=="POST")
            {
               try
                {
                    string request = Request.Params["request"].ToString();
                    switch(request)
                    {
                        case "login":
                            login();
                            break;
                        case "emplist":
                            emplist();
                            break;
                        case "empcount":
                            empcount();
                            break;
                        case "preabscount":
                            preabscount();
                            break;
                        case "absentdetails":
                            absentdetails();
                            break;
                        case "presentdetails":
                            presentdetails();
                            break;
                        case "earlycomes":
                            earlycomes();
                            break;
                        case "latecomes":
                            latecomes();
                            break;
                        case "earlygoes":
                            earlygoes();
                            break;
                        case "lategoes":
                            lategoes();
                            break;
                        case "empmonthreports":
                            empmonthreports();
                            break;
                        case "emptwodatesreports":
                            emptwodatesreports();
                            break;
                        case "empmonthcount":
                            empmonthcount();
                            break;
                        case "emptwodatecount":
                            emptwodatecount();
                            break;
                        case "demo":
                            demo();
                            break;
                        case "alumni":
                            alumni();
                            break;
                        case "listdisplay":
                            listdisp();
                            break;



                    }
                }
                catch (Exception ex)
                {
                    Response.Write("{\"error\": true,\"result\":\"Server Error=" + ex.Message.ToString() + "\"}");
                }
            }
        }
        public string DatatableToJsonResults(DataTable  table)
        {

            string jsonstr = string.Empty;
            jsonstr = Newtonsoft.Json.JsonConvert.SerializeObject(table);
            return jsonstr;
        }


        public void login()
        {
            string uname = Request.Params["Admin"].ToString();
            string pwd = Request.Params["password"].ToString();
            string Name = string.Empty;

            DataSet ds = DataQueries.Selectcomm("select * from register where Admin='"+uname+"' and password='"+pwd+"'");
            if(ds.Tables[0].Rows.Count>0)
            {

                 Name = ds.Tables[0].Rows[0]["Name"].ToString();
                Response.Write("{\"error\": false,\"message\":\"Success\",\"Name\":\"" + Name + "\"}");
            }
      
          
        }

        public void emplist()
        {

            DataSet ds = DataQueries.Selectcomm("select EmployeeId,EmployeName from Employees");
            DataTable table = ds.Tables[0];
            string json = DatatableToJsonResults(table);
            Response.Write(json);

        }
        public void empcount()
        {
            string eccount = string.Empty;
            string lccount = string.Empty;
            string egcount = string.Empty;
            string lgcount = string.Empty;

            DataQueries.Insertcomm("EXEC [dbo].[Proc_fullattendance] @StartDate = '"+DateTime.Now.ToString("yyyy-MM-dd")+"'");
            DataSet ds = DataQueries.Selectcomm("select  count(EarlyComes)  as earlycomes,(Select count(LateComes) as latecomes from AttendanceLogs1 where latecomes!='') as lcm,(select COUNT(EarlyGoes) as earlygoes from AttendanceLogs1 where earlygoes!='') as eg,(select COUNT(LateGoes) as lategoes from AttendanceLogs1 where lategoes!='') as lg from AttendanceLogs1 where EarlyComes!=''");
            if(ds.Tables[0].Rows.Count>0)
            {
                eccount = ds.Tables[0].Rows[0]["earlycomes"].ToString();
                lccount = ds.Tables[0].Rows[0]["lcm"].ToString();
                egcount = ds.Tables[0].Rows[0]["eg"].ToString();
                lgcount = ds.Tables[0].Rows[0]["lg"].ToString();

            }
            Response.Write("{\"error\": false,\"message\":\"Success\",\"earlycomes\":\"" + eccount + "\",\"lcm\":\"" + lccount + "\",\"eg\":\"" + egcount + "\",\"lg\":\"" + lgcount + "\"}");
        }
        public void preabscount()
        {
            string present = string.Empty;
            string absent = string.Empty;
            string latecomes = string.Empty;
            string empcount = string.Empty;
            DataQueries.Insertcomm("EXEC [dbo].[Proc_fullattendance] @StartDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            DataSet ds = DataQueries.Selectcomm("select (select count(Detailedstatus) as present from AttendanceLogs1 where DetailedStatus='p') as presentcount,(select count(Detailedstatus) as absentcount from AttendanceLogs1 where DetailedStatus='A') as absentcount,(select count(LateComes) as latecomes from AttendanceLogs1 where LateComes!='') as latecomes,(select count(*) from Employees) as empcount");
            if(ds.Tables[0].Rows.Count>0)
            {
                present = ds.Tables[0].Rows[0]["presentcount"].ToString();
                absent = ds.Tables[0].Rows[0]["absentcount"].ToString();
                latecomes= ds.Tables[0].Rows[0]["latecomes"].ToString();
                empcount= ds.Tables[0].Rows[0]["empcount"].ToString();

            }
            Response.Write("{\"error\": false,\"message\":\"Success\",\"presentcount\":\"" + present + "\",\"absentcount\":\"" + absent + "\",\"latecomes\":\"" + latecomes + "\",\"empcount\":\"" + empcount + "\"}");


        }

        public void absentdetails()
        {
            DataQueries.Insertcomm("EXEC [dbo].[Proc_fullattendance] @StartDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            DataSet ds = DataQueries.Selectcomm("select EmployeeName,Desigantion11,Detailedstatus from AttendanceLogs1 where DetailedStatus='A'");
            DataTable table = ds.Tables[0];
            string json = DatatableToJsonResults(table);
            Response.Write(json);

        }
        public void presentdetails()
        {              
            DataQueries.Insertcomm("EXEC [dbo].[Proc_fullattendance] @StartDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            DataSet ds = DataQueries.Selectcomm("select EmployeeName,Desigantion11,Detailedstatus,intime,OutTime,Duration from AttendanceLogs1 where DetailedStatus='p'");
            DataTable table = ds.Tables[0];
            string json = DatatableToJsonResults(table);
            Response.Write(json);

        }
        protected void latecomes()
        {
            DataQueries.Insertcomm("EXEC [dbo].[Proc_fullattendance] @StartDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            DataSet ds = DataQueries.Selectcomm("select EmployeeName,Desigantion11,intime,OutTime,LateComes from AttendanceLogs1 where latecomes!=''");
            DataTable table = ds.Tables[0];
            string json = DatatableToJsonResults(table);
            Response.Write(json);


        }
        protected void earlycomes()
        {
            DataQueries.Insertcomm("EXEC [dbo].[Proc_fullattendance] @StartDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            DataSet ds = DataQueries.Selectcomm("select EmployeeName,Desigantion11,intime,OutTime,EarlyComes from AttendanceLogs1 where EarlyComes!=''");
            DataTable table = ds.Tables[0];
            string json = DatatableToJsonResults(table);
            Response.Write(json);

        }
        protected void earlygoes()
        {
            DataQueries.Insertcomm("EXEC [dbo].[Proc_fullattendance] @StartDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            DataSet ds = DataQueries.Selectcomm("select EmployeeName,Desigantion11,intime,OutTime,EarlyGoes from AttendanceLogs1 where EarlyGoes!=''");
            DataTable table = ds.Tables[0];
            string json = DatatableToJsonResults(table);
            Response.Write(json);

        }
        protected void lategoes()
        {
            DataQueries.Insertcomm("EXEC [dbo].[Proc_fullattendance] @StartDate = '" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            DataSet ds = DataQueries.Selectcomm("select EmployeeName,Desigantion11,intime,OutTime,LateGoes from AttendanceLogs1 where LateGoes!=''");
            DataTable table = ds.Tables[0];
            string json = DatatableToJsonResults(table);
            Response.Write(json);

        }
        protected void empmonthreports()
        {
            string date = Request.Params["Date"].ToString();
            string eid = Request.Params["empid"].ToString();
            DataQueries.Insertcomm("EXEC [dbo].[Proc_Monthlydates] @StartDate = '"+date+"',@Company = null,@Department = null,@EmpName = '"+eid+"',@Basic = 'Basic'");
            DataSet ds = DataQueries.Selectcomm("select cast(AttendanceDate as varchar(10)) as AttendanceDate,InTime,OutTime,Duration from AttendanceLogs1 where EmployeeId='" + eid+"'");
            DataTable table = ds.Tables[0];
            string json = DatatableToJsonResults(table);
            Response.Write(json);



        }
        protected void emptwodatesreports()
        {
           string eid = Request.Params["empid"].ToString();
            string sdate = Request.Params["sdate"].ToString();
            string edate = Request.Params["edate"].ToString();
            DataQueries.Insertcomm("EXEC [dbo].[Proc_Twodates]@StartDate = '"+sdate+"', @Company = null, @Department = null, @EmpName = '"+eid+"',@EndDate = '"+edate+"',@Basic = 'Basic'");
            DataSet ds = DataQueries.Selectcomm("select cast(AttendanceDate as varchar(10)) as AttendanceDate,InTime,OutTime,Duration from AttendanceLogs1 where EmployeeId='" + eid+"'");
            DataTable table = ds.Tables[0];
            string json = DatatableToJsonResults(table);
            Response.Write(json);

        }
        protected void empmonthcount()
        {
            string date = Request.Params["Date"].ToString();
            string eid = Request.Params["empid"].ToString();
            DataQueries.Insertcomm("EXEC [dbo].[Proc_Monthlydates] @StartDate = '" + date + "',@Company = null,@Department = null,@EmpName = '" + eid + "',@Basic = 'Basic'");
            DataSet ds = DataQueries.Selectcomm("select Desigantion11,count(AttendanceDate) as TotalDays,(select count(detailedstatus) from AttendanceLogs1 where detailedstatus='A' and employeeid= '" + eid + "' ) as absent,(select count(DetailedStatus ) from attendancelogs1  where DetailedStatus='p' and employeeid= '" + eid + "' ) as Present,(select count(LateComes) from AttendanceLogs1 where LateComes!='' and EmployeeID='"+eid+"') as latecomes from attendancelogs1 where employeeid= '" + eid+"' group by Desigantion11");
            DataTable table = ds.Tables[0];
            string json = DatatableToJsonResults(table);
            Response.Write(json);

        }
        protected void emptwodatecount()
        {

            string eid = Request.Params["empid"].ToString();
            string sdate = Request.Params["sdate"].ToString();
            string edate = Request.Params["edate"].ToString();
            DataQueries.Insertcomm("EXEC [dbo].[Proc_Twodates]@StartDate = '" + sdate + "', @Company = null, @Department = null, @EmpName = '" + eid + "',@EndDate = '" + edate + "',@Basic = 'Basic'");
            DataSet ds = DataQueries.Selectcomm("select Desigantion11,(select count(detailedstatus) from AttendanceLogs1 where detailedstatus='A' and employeeid= '" + eid + "' ) as absent,(select count(DetailedStatus ) from attendancelogs1  where DetailedStatus='p' and employeeid= '" + eid + "' ) as Present,(select count(LateComes) from AttendanceLogs1 where LateComes!='' and EmployeeID='" + eid + "') as latecomes from attendancelogs1 where employeeid= '" + eid+"'  group by Desigantion11 ");
            DataTable table = ds.Tables[0];
            string json = DatatableToJsonResults(table);
            Response.Write(json);
        }
        protected void demo()
        {
            string date = Request.Params["date"].ToString();
            string mobile = Request.Params["mobile"].ToString();
            DataQueries.Insertcomm("insert into demo(date,mobile) values('"+date+"','"+mobile+"')");
            Response.Write("{\"error\": false,\"message\":\"Success\"}");

        }

        string txtfname = string.Empty;
        protected void alumni()
        {
            txtfname  = Request.Params["txtfname"].ToString();

            string mobile = Request.Params["mobile"].ToString();
            string email = Request.Params["email"].ToString();
            string txtmobile = Request.Params["txtmobile"].ToString();
            string gender = Request.Params["gender"].ToString();
            string qualify = Request.Params["qualify"].ToString();
            string designation = Request.Params["designation"].ToString();
            string company = Request.Params["company"].ToString();
            string role = Request.Params["role"].ToString();
            string txtaddress = Request.Params["txtaddress"].ToString();
            string dob = Request.Params["dob"].ToString();
            string mars = Request.Params["marstatus"].ToString();
            string mdate = Request.Params["mdate"].ToString();
            string kids = Request.Params["kids"].ToString();
            string kidsname = Request.Params["kidsname"].ToString();
            string ssc = Request.Params["ssc"].ToString();
            string image = Request.Params["image"].ToString();
            byte[] bytes1 = Convert.FromBase64String(image);
            File.WriteAllBytes(Server.MapPath("~/images/") + mobile + ".jpg", bytes1);
            string path = "http://vms.bterp.in/images/" + mobile + ".jpg";
            
            string constr = ConfigurationManager.ConnectionStrings["alumni"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            con.Open();
            SqlCommand cmd = new SqlCommand("insert into alumnimeet([name],[mobile],[email],[phone],[gender],[qualification],[job],[company],[designation],[address],[dob],[marital],[anniversary],[kids],[kidsname],[passout],image) values" +
                " ('" + txtfname + "','" + mobile + "','" + email + "','" + txtmobile + "','" + gender + "','" + qualify + "','" + designation + "','" + company + "','" + role + "','" + txtaddress + "','" + dob+ "','" + mars + "','" + mdate+ "','" + kids + "','" + kidsname+ "','" + ssc + "','" + path + "')", con);
            cmd.ExecuteNonQuery();
            con.Close();
            sendOTP(mobile);
            Response.Write("{\"error\": false,\"message\":\"Success\"}");
        }

        public void sendOTP(string mobile)
        {
            WebRequest request = null;
            HttpWebResponse response = null;
            String userid = "BTRAK";
            String passwd = "841090";
            string smstext = "Dear Mr./Mrs. " + txtfname + " Thank you for registering your details with ZPHS Pedakapavaram.We will get back to you with the updates regarding developements of the school";
            String url = "http://113.193.191.132/smpp/?username=" + userid + "&password=" + passwd + "&from=BTVIMS&to=91" + mobile + "&text=" + smstext + "";
            request = WebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();
        }

        protected void listdisp()
        {
            //DataSet ds = DataQueries.Selectcomm("select * from alumnimeet order by id desc ");
            string constr = ConfigurationManager.ConnectionStrings["alumni"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from alumnimeet order by id desc",con);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            DataTable table = ds.Tables[0];
            string json = DatatableToJsonResults(table);
            Response.Write(json);

        }
    }
}